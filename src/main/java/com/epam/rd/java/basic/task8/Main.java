package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.entity.Constants;
import com.epam.rd.java.basic.task8.entity.Flower;
import com.epam.rd.java.basic.task8.controller.*;
import org.xml.sax.XMLReader;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import java.util.ArrayList;
import java.util.Comparator;

public class Main {

    public static void main(String[] args) throws Exception {
        if (args.length != 1) {
            return;
        }

        String xmlFileName = args[0];
        ArrayList<Flower> flowerList;

        DOMController domController = new DOMController(xmlFileName);
        flowerList = domController.DOM();
        System.out.println("DOM");
        Constants.outputFlowersList(flowerList);
        flowerList.sort(Comparator.comparingInt(f -> f.getGrowingTips().getTemperature()));
        DOMController.writeXml(flowerList);


        SAXParserFactory spf = SAXParserFactory.newInstance();
        SAXParser saxParser = spf.newSAXParser();
        XMLReader xmlReader = saxParser.getXMLReader();
        SAXController saxController = new SAXController(xmlFileName);
        xmlReader.setContentHandler(saxController);
        xmlReader.parse("input.xml");

        flowerList = saxController.getResult();
        flowerList.sort(Comparator.comparingInt(f -> f.getVisualParameters().getAveLenFlower()));
        SAXController.writeXml(flowerList);

        STAXController staxController = new STAXController(xmlFileName);
        flowerList = staxController.parse();
        flowerList.sort(Comparator.comparing(Flower::getName));
        STAXController.writeXml(flowerList);
    }

}