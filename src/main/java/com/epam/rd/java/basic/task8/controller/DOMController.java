package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.entity.*;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;


import java.io.OutputStream;

import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;


import static com.epam.rd.java.basic.task8.entity.Constants.*;

/**
 * Controller for DOM parser.
 */
public class DOMController {

    private String xmlFileName;

    public DOMController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    public ArrayList<Flower> DOM() {
        File file = new File(xmlFileName);
        Flower fl = new Flower();
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        Document doc = null;
        try {
            doc = dbf.newDocumentBuilder().parse(file);
        } catch (SAXException | IOException | ParserConfigurationException e) {
            e.printStackTrace();
        }

        Node rootNode = doc.getFirstChild();
        NodeList rootChilds = rootNode.getChildNodes();

        ArrayList<Flower> list = new ArrayList<>();

        for (int i = 0; i < rootChilds.getLength(); i++) {
            if (rootChilds.item(i).getNodeType() != Node.ELEMENT_NODE) {
                continue;
            }

            if (!rootChilds.item(i).getNodeName().equals("flower")) {
                continue;
            }

            String name = "name";
            Soil soil = null;
            String origin = "origin";
            VisualParameters visualParameters = null;
            GrowingTips growingTips = null;
            Multiplying multiplying = null;

            NodeList elementChilds = rootChilds.item(i).getChildNodes();
            for (int j = 0; j < elementChilds.getLength(); j++) {

                if (elementChilds.item(j).getNodeType() != Node.ELEMENT_NODE) {
                    continue;
                }

                switch (elementChilds.item(j).getNodeName()) {
                    case NAME_TAG:
                        name = elementChilds.item(j).getTextContent();
                        break;

                    case SOIL_TAG:
                        soil = Soil.determineSoil(elementChilds.item(j).getTextContent());
                        break;

                    case ORIGIN_TAG:
                        origin = elementChilds.item(j).getTextContent();
                        break;

                    case VISUAL_PARAMETERS_TAG:
                        String stemColour = null;
                        String leafColour = null;
                        int aveLenFlower = 0;

                        NodeList visualParametersNode = elementChilds.item(j).getChildNodes();

                        for (int k = 0; k < visualParametersNode.getLength(); k++) {

                            if (elementChilds.item(k).getNodeType() != Node.ELEMENT_NODE) {
                                continue;
                            }

                            switch (visualParametersNode.item(k).getNodeName()) {
                                case STEM_COLOUR_TAG:
                                    stemColour = visualParametersNode.item(k).getTextContent();
                                    break;
                                case LEAF_COLOUR_TAG:
                                    leafColour = visualParametersNode.item(k).getTextContent();
                                    break;
                                case AVE_LEN_FLOWER_TAG:
                                    aveLenFlower = Integer.parseInt(visualParametersNode.item(k).getTextContent());
                                    break;
                            }
                        }
                        visualParameters = new VisualParameters(stemColour, leafColour, aveLenFlower);

                    case GROWING_TIPS_TAG:
                        int temperature = 0;
                        Lighting lighting = null;
                        int watering = 0;

                        NodeList growingTipsNode = elementChilds.item(j).getChildNodes();

                        for (int k = 0; k < growingTipsNode.getLength(); k++) {
                            if (elementChilds.item(k).getNodeType() != Node.ELEMENT_NODE) {
                                continue;
                            }

                            switch (growingTipsNode.item(k).getNodeName()) {
                                case TEMPERATURE_TAG:
                                    temperature = Integer.parseInt(growingTipsNode.item(k).getTextContent());
                                    break;
                                case LIGHTING_TAG:
                                    lighting = Lighting.determineLighting(growingTipsNode.item(k)
                                            .getAttributes().getNamedItem("lightRequiring").getNodeValue());
                                    break;
                                case WATERING_TAG:
                                    watering = Integer.parseInt(growingTipsNode.item(k).getTextContent());
                            }
                        }
                        growingTips = new GrowingTips(temperature, lighting, watering);

                    case MULTIPLYING_TAG:
                        multiplying = Multiplying.determineMultiplying(elementChilds.item(j).getTextContent());
                }
            }
            list.add(new Flower(name, soil, origin, visualParameters, growingTips, multiplying));
        }
        return list;
    }


    public static void writeXml(ArrayList<Flower> flowerList) throws ParserConfigurationException, TransformerException, FileNotFoundException {
        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

        // root elements
        Document doc = docBuilder.newDocument();
        Element rootElement = doc.createElement("flowers");
        rootElement.setAttribute("xmlns", "http://www.nure.ua");
        rootElement.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
        rootElement.setAttribute("xsi:schemaLocation", "http://www.nure.ua input.xsd ");

        doc.appendChild(rootElement);

        for (Flower value : flowerList) {


            Element flower = doc.createElement("flower");
            rootElement.appendChild(flower);

            Element name = doc.createElement("name");
            name.setTextContent(value.getName());
            flower.appendChild(name);

            Element soil = doc.createElement("soil");
            soil.setTextContent(value.getSoil().getType());
            flower.appendChild(soil);

            Element origin = doc.createElement("origin");
            origin.setTextContent(value.getOrigin());
            flower.appendChild(origin);

            Element visualParameters = doc.createElement("visualParameters");
            flower.appendChild(visualParameters);

            Element stemColour = doc.createElement("stemColour");
            stemColour.setTextContent(value.getVisualParameters().getStemColour());
            visualParameters.appendChild(stemColour);

            Element leafColour = doc.createElement("leafColour");
            leafColour.setTextContent(value.getVisualParameters().getLeafColour());
            visualParameters.appendChild(leafColour);

            Element aveLenFlower = doc.createElement("aveLenFlower");
            aveLenFlower.setAttribute("measure", "cm");
            aveLenFlower.setTextContent(String.valueOf(value.getVisualParameters().getAveLenFlower()));
            visualParameters.appendChild(aveLenFlower);

            Element growingTips = doc.createElement("growingTips");
            flower.appendChild(growingTips);

            Element tempreture = doc.createElement("tempreture");
            tempreture.setAttribute("measure", "celcius");
            tempreture.setTextContent(String.valueOf(value.getGrowingTips().getTemperature()));
            growingTips.appendChild(tempreture);

            Element lighting = doc.createElement("lighting");
            lighting.setAttribute("lightRequiring", value.getGrowingTips().getLighting().getRequired());
            growingTips.appendChild(lighting);

            Element watering = doc.createElement("watering");
            watering.setAttribute("measure", "mlPerWeek");
            watering.setTextContent(String.valueOf(value.getGrowingTips().getWatering()));
            growingTips.appendChild(watering);

            Element multiplying = doc.createElement("multiplying");
            multiplying.setTextContent(value.getMultiplying().getType());
            flower.appendChild(multiplying);

        }

        prettyOutput(doc, new FileOutputStream("output.dom.xml"));
    }

    public static void prettyOutput(Document doc, OutputStream output) throws TransformerException {
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();

        transformer.setOutputProperty(OutputKeys.INDENT, "yes");

        DOMSource source = new DOMSource(doc);
        StreamResult result = new StreamResult(output);

        transformer.transform(source, result);
    }

}

