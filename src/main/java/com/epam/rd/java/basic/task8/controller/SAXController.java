package com.epam.rd.java.basic.task8.controller;

import org.xml.sax.helpers.DefaultHandler;
import com.epam.rd.java.basic.task8.entity.*;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import javax.xml.transform.TransformerException;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;

import static com.epam.rd.java.basic.task8.entity.Constants.*;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {

    private ArrayList<Flower> flowerList = null;
    private Flower currentFlower;
    private String currentElement;

    public SAXController(String xmlFileName) {
    }

    public ArrayList<Flower> getResult() {
        return flowerList;
    }


    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        currentElement = qName;
        switch (currentElement) {
            case FLOWERS_TAG:
                flowerList = new ArrayList<>();
                break;
            case FLOWER_TAG:
                currentFlower = new Flower();
                break;

            case VISUAL_PARAMETERS_TAG:
                VisualParameters currentVisualParameters = new VisualParameters();
                currentFlower.setVisualParameters(currentVisualParameters);
                break;

            case GROWING_TIPS_TAG:
                GrowingTips currentGrowingTips = new GrowingTips();
                currentFlower.setGrowingTips(currentGrowingTips);
                break;
            case LIGHTING_TAG:
                currentFlower.getGrowingTips()
                        .setLighting(Lighting.determineLighting(attributes.getValue(LIGHT_REQUIRING_TAG)));
                break;

        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        String text = new String(ch, start, length);

        if (text.contains("<") || currentElement == null) {
            return;
        }

        switch (currentElement) {
            case NAME_TAG:
                currentFlower.setName(text);
                break;
            case SOIL_TAG:
                currentFlower.setSoil(Soil.determineSoil(text));
                break;
            case ORIGIN_TAG:
                currentFlower.setOrigin(text);
                break;
            case STEM_COLOUR_TAG:
                currentFlower.getVisualParameters().setStemColour(text);
                break;
            case LEAF_COLOUR_TAG:
                currentFlower.getVisualParameters().setLeafColour(text);
                break;
            case AVE_LEN_FLOWER_TAG:
                currentFlower.getVisualParameters().setAveLenFlower(Integer.parseInt(text));
                break;
            case TEMPERATURE_TAG:
                currentFlower.getGrowingTips().setTemperature(Integer.parseInt(text));
                break;
            case WATERING_TAG:
                currentFlower.getGrowingTips().setWatering(Integer.parseInt(text));
                break;
            case MULTIPLYING_TAG:
                currentFlower.setMultiplying(Multiplying.determineMultiplying(text));
                break;
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (FLOWER_TAG.equals(qName)) {
            if (currentFlower != null) {
                flowerList.add(currentFlower);
                currentFlower = null;
            }
        }
        currentElement = null;
    }

    @Override
    public void endDocument() throws SAXException {
        System.out.println("SAX");
        outputFlowersList(flowerList);
    }

    public static void writeXml(ArrayList<Flower> flowerList) throws XMLStreamException, FileNotFoundException, TransformerException {
        XMLOutputFactory output = XMLOutputFactory.newInstance();

        XMLStreamWriter writer = output.createXMLStreamWriter(new FileOutputStream("output.sax.xml"));

        writer.writeStartDocument("utf-8", "1.0");

        writer.writeStartElement("flowers");
        writer.writeAttribute("xmlns", "http://www.nure.ua");
        writer.writeAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
        writer.writeAttribute("xsi:schemaLocation", "http://www.nure.ua input.xsd ");

        for (Flower flower : flowerList) {

            writer.writeStartElement("flower");

            writer.writeStartElement("name");
            writer.writeCharacters(flower.getName());
            writer.writeEndElement();

            writer.writeStartElement("soil");
            writer.writeCharacters(flower.getSoil().getType());
            writer.writeEndElement();

            writer.writeStartElement("origin");
            writer.writeCharacters(flower.getOrigin());
            writer.writeEndElement();

            writer.writeStartElement("visualParameters");

            writer.writeStartElement("stemColour");
            writer.writeCharacters(flower.getVisualParameters().getStemColour());
            writer.writeEndElement();

            writer.writeStartElement("leafColour");
            writer.writeCharacters(flower.getVisualParameters().getLeafColour());
            writer.writeEndElement();

            writer.writeStartElement("aveLenFlower");
            writer.writeAttribute("measure", "cm");
            writer.writeCharacters(String.valueOf(flower.getVisualParameters().getAveLenFlower()));
            writer.writeEndElement();

            writer.writeEndElement();

            writer.writeStartElement("growingTips");

            writer.writeStartElement("tempreture");
            writer.writeAttribute("measure", "celcius");
            writer.writeCharacters(String.valueOf(flower.getGrowingTips().getTemperature()));
            writer.writeEndElement();

            writer.writeEmptyElement("lighting");
            writer.writeAttribute("lightRequiring", flower.getGrowingTips().getLighting().getRequired());

            writer.writeStartElement("watering");
            writer.writeAttribute("measure", "mlPerWeek");
            writer.writeCharacters(String.valueOf(flower.getGrowingTips().getWatering()));
            writer.writeEndElement();

            writer.writeEndElement();

            writer.writeStartElement("multiplying");
            writer.writeCharacters(flower.getMultiplying().getType());
            writer.writeEndElement();

            writer.writeEndElement();
        }

        writer.writeEndElement();

        writer.writeEndDocument();

        writer.flush();
        writer.close();

    }
}