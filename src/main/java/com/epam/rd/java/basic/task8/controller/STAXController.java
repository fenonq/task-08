package com.epam.rd.java.basic.task8.controller;


import java.io.*;
import java.util.ArrayList;

import javax.xml.stream.*;
import javax.xml.stream.events.XMLEvent;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.namespace.QName;

import javax.xml.stream.XMLStreamException;
import javax.xml.transform.TransformerException;

import org.xml.sax.helpers.DefaultHandler;

import static com.epam.rd.java.basic.task8.entity.Constants.*;
import com.epam.rd.java.basic.task8.entity.*;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

	private static String xmlFileName;

	public STAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public ArrayList<Flower> parse() {
		ArrayList<Flower> flowerList = new ArrayList<Flower>();
		Flower currentFlower = null;
		XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
		try {
			XMLEventReader reader = xmlInputFactory.createXMLEventReader(new FileInputStream(xmlFileName));
			while (reader.hasNext()) {
				XMLEvent nextEvent = reader.nextEvent();
				if (nextEvent.isStartElement()) {
					StartElement startElement = nextEvent.asStartElement();
					switch (startElement.getName().getLocalPart()) {
						case FLOWER_TAG:
							currentFlower = new Flower();
							break;
						case NAME_TAG:
							nextEvent = reader.nextEvent();
							currentFlower.setName(nextEvent.asCharacters().getData());
							break;
						case SOIL_TAG:
							nextEvent = reader.nextEvent();
							currentFlower.setSoil(Soil.determineSoil(nextEvent.asCharacters().getData()));
							break;
						case ORIGIN_TAG:
							nextEvent = reader.nextEvent();
							currentFlower.setOrigin(nextEvent.asCharacters().getData());
							break;
						case VISUAL_PARAMETERS_TAG:
							VisualParameters currentVisualParameters = new VisualParameters();
							currentFlower.setVisualParameters(currentVisualParameters);
							break;
						case GROWING_TIPS_TAG:
							GrowingTips currentGrowingTips = new GrowingTips();
							currentFlower.setGrowingTips(currentGrowingTips);
							break;
						case STEM_COLOUR_TAG:
							nextEvent = reader.nextEvent();
							currentFlower.getVisualParameters().setStemColour(nextEvent.asCharacters().getData());
							break;
						case LEAF_COLOUR_TAG:
							nextEvent = reader.nextEvent();
							currentFlower.getVisualParameters().setLeafColour(nextEvent.asCharacters().getData());
							break;
						case AVE_LEN_FLOWER_TAG:
							nextEvent = reader.nextEvent();
							currentFlower.getVisualParameters().setAveLenFlower(Integer.parseInt(nextEvent.asCharacters().getData()));
							break;
						case TEMPERATURE_TAG:
							nextEvent = reader.nextEvent();
							currentFlower.getGrowingTips().setTemperature(Integer.parseInt(nextEvent.asCharacters().getData()));
							break;
						case LIGHTING_TAG:
							nextEvent = reader.nextEvent();
							Attribute lighting = startElement.getAttributeByName(new QName(LIGHT_REQUIRING_TAG));
							if (lighting != null) {
								currentFlower.getGrowingTips().setLighting(Lighting.determineLighting(lighting.getValue()));
							}
							break;
						case WATERING_TAG:
							nextEvent = reader.nextEvent();
							currentFlower.getGrowingTips().setWatering(Integer.parseInt(nextEvent.asCharacters().getData()));
							break;
						case MULTIPLYING_TAG:
							nextEvent = reader.nextEvent();
							currentFlower.setMultiplying(Multiplying.determineMultiplying(nextEvent.asCharacters().getData()));
							break;
					}

				}
				if (nextEvent.isEndElement()) {
					EndElement endElement = nextEvent.asEndElement();
					if (endElement.getName()
							.getLocalPart()
							.equals(FLOWER_TAG)) {
						flowerList.add(currentFlower);
					}
				}
			}
		} catch (XMLStreamException xse) {
			System.out.println("XMLStreamException");
			xse.printStackTrace();
		} catch (FileNotFoundException fnfe) {
			System.out.println("FileNotFoundException");
			fnfe.printStackTrace();
		}

		System.out.println("STAX");
		outputFlowersList(flowerList);
		return flowerList;
	}

	public static void writeXml(ArrayList<Flower> flowerList) throws XMLStreamException, FileNotFoundException, TransformerException {
		XMLOutputFactory output = XMLOutputFactory.newInstance();

		XMLStreamWriter writer = output.createXMLStreamWriter(new FileOutputStream("output.stax.xml"));

		writer.writeStartDocument("utf-8", "1.0");

		writer.writeStartElement("flowers");
		writer.writeAttribute("xmlns", "http://www.nure.ua");
		writer.writeAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
		writer.writeAttribute("xsi:schemaLocation", "http://www.nure.ua input.xsd ");

		for (Flower flower : flowerList) {

			writer.writeStartElement("flower");

			writer.writeStartElement("name");
			writer.writeCharacters(flower.getName());
			writer.writeEndElement();

			writer.writeStartElement("soil");
			writer.writeCharacters(flower.getSoil().getType());
			writer.writeEndElement();

			writer.writeStartElement("origin");
			writer.writeCharacters(flower.getOrigin());
			writer.writeEndElement();

			writer.writeStartElement("visualParameters");

			writer.writeStartElement("stemColour");
			writer.writeCharacters(flower.getVisualParameters().getStemColour());
			writer.writeEndElement();

			writer.writeStartElement("leafColour");
			writer.writeCharacters(flower.getVisualParameters().getLeafColour());
			writer.writeEndElement();

			writer.writeStartElement("aveLenFlower");
			writer.writeAttribute("measure", "cm");
			writer.writeCharacters(String.valueOf(flower.getVisualParameters().getAveLenFlower()));
			writer.writeEndElement();

			writer.writeEndElement();

			writer.writeStartElement("growingTips");

			writer.writeStartElement("tempreture");
			writer.writeAttribute("measure", "celcius");
			writer.writeCharacters(String.valueOf(flower.getGrowingTips().getTemperature()));
			writer.writeEndElement();

			writer.writeEmptyElement("lighting");
			writer.writeAttribute("lightRequiring", flower.getGrowingTips().getLighting().getRequired());

			writer.writeStartElement("watering");
			writer.writeAttribute("measure", "mlPerWeek");
			writer.writeCharacters(String.valueOf(flower.getGrowingTips().getWatering()));
			writer.writeEndElement();

			writer.writeEndElement();

			writer.writeStartElement("multiplying");
			writer.writeCharacters(flower.getMultiplying().getType());
			writer.writeEndElement();

			writer.writeEndElement();
		}

		writer.writeEndElement();

		writer.writeEndDocument();

		writer.flush();
		writer.close();
	}
}